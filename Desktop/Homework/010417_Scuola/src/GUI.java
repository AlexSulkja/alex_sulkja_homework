import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JSeparator;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import java.awt.GridLayout;

public class GUI {
	private static JTextField textField;
	private static JTextField textField_1;
	private static JTextField textField_2;
	private static JTextField textField_3;
	private static JTextField textField_4;
	private static JTextField textField_5;
	private static JTextField textField_6;
	private static JTextField textField_7;
	private static JTextField textField_8;
	
	
	public static void main(String [] args){
		String classe[]= {"1","2","3","4","5"};
		String sezione[]= {"A","B","C","D","E","F"};
		String indirizzo[]= {"Informatica","Elettronica","Elettrotecnica","Meccanica","Occhiale","Edile","null"};
		
		JFrame jfrm = new JFrame("Scuola");
		jfrm.setSize(400, 404);
		jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		jfrm.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel scuolaP = new JPanel();
		tabbedPane.addTab("Scuola", null, scuolaP, null);
		scuolaP.setLayout(null);
		
		JLabel lblAnnoScolastico = new JLabel("Anno Scolastico");
		lblAnnoScolastico.setBounds(27, 51, 90, 14);
		scuolaP.add(lblAnnoScolastico);
		
		textField = new JTextField();
		textField.setBounds(143, 48, 212, 20);
		scuolaP.add(textField);
		textField.setColumns(10);
		
		JLabel lblClasse = new JLabel("Classe");
		lblClasse.setBounds(27, 98, 46, 14);
		scuolaP.add(lblClasse);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Seleziona la classe...", "1", "2", "3", "4", "5"}));
		comboBox.setBounds(143, 95, 212, 20);
		scuolaP.add(comboBox);
		
		JLabel lblSezione = new JLabel("Sezione ");
		lblSezione.setBounds(27, 149, 46, 14);
		scuolaP.add(lblSezione);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Seleziona la sezione...", "A", "B", "C", "D", "E", "F"}));
		comboBox_1.setBounds(143, 146, 212, 20);
		scuolaP.add(comboBox_1);
		
		JLabel lblIndirizzo = new JLabel("Indirizzo");
		lblIndirizzo.setBounds(27, 198, 46, 14);
		scuolaP.add(lblIndirizzo);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"Seleziona l'indirizzo...", "Informatica ", "Occhiale", "Edile ", "Meccanica ", "Elettronica ", "Elettrotecnica"}));
		comboBox_2.setBounds(143, 195, 212, 20);
		scuolaP.add(comboBox_2);
		
		JButton btnNewButton = new JButton("INVIA");
		btnNewButton.setBounds(143, 257, 95, 32);
		scuolaP.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConnectDB_1 db = new ConnectDB_1(textField.getText(), comboBox.getSelectedItem(), comboBox_1.getSelectedItem(), comboBox_2.getSelectedItem());
			}
		});
		
		JLabel lblSegreteria = new JLabel("SEGRETERIA ITIS");
		lblSegreteria.setBounds(143, 11, 103, 14);
		scuolaP.add(lblSegreteria);
		
		JPanel alunnoP = new JPanel();
		tabbedPane.addTab("Alunno", null, alunnoP, null);
		alunnoP.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome ");
		lblNome.setBounds(18, 31, 46, 14);
		alunnoP.add(lblNome);
		
		textField_1 = new JTextField();
		textField_1.setBounds(125, 28, 230, 20);
		alunnoP.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblCognome = new JLabel("Cognome");
		lblCognome.setBounds(18, 56, 46, 14);
		alunnoP.add(lblCognome);
		
		textField_2 = new JTextField();
		textField_2.setBounds(125, 53, 230, 20);
		alunnoP.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblDataDiNascita = new JLabel("Data di nascita");
		lblDataDiNascita.setBounds(18, 81, 78, 14);
		alunnoP.add(lblDataDiNascita);
		
		textField_3 = new JTextField();
		textField_3.setBounds(125, 78, 230, 20);
		alunnoP.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblIndirizzo_1 = new JLabel("Indirizzo");
		lblIndirizzo_1.setBounds(18, 181, 46, 14);
		alunnoP.add(lblIndirizzo_1);
		
		textField_4 = new JTextField();
		textField_4.setBounds(125, 203, 230, 20);
		alunnoP.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblNumeroDiTelefono = new JLabel("Numero di telefono");
		lblNumeroDiTelefono.setBounds(18, 232, 91, 14);
		alunnoP.add(lblNumeroDiTelefono);
		
		textField_5 = new JTextField();
		textField_5.setBounds(125, 229, 230, 20);
		alunnoP.add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblNazionalitItaliana = new JLabel("Nazionalit\u00E0 italiana");
		lblNazionalitItaliana.setBounds(18, 106, 91, 14);
		alunnoP.add(lblNazionalitItaliana);
		
		JComboBox comboBox_3 = new JComboBox();
		comboBox_3.setModel(new DefaultComboBoxModel(new String[] {"SI/NO...", "Si", "No"}));
		comboBox_3.setBounds(125, 103, 230, 20);
		alunnoP.add(comboBox_3);
		
		JLabel lblComuneDiNascita = new JLabel("Comune di nascita");
		lblComuneDiNascita.setBounds(18, 131, 91, 14);
		alunnoP.add(lblComuneDiNascita);
		
		textField_8 = new JTextField();
		textField_8.setBounds(125, 128, 230, 20);
		alunnoP.add(textField_8);
		textField_8.setColumns(10);
		
		JLabel lblProvincia = new JLabel("Provincia");
		lblProvincia.setBounds(18, 156, 46, 14);
		alunnoP.add(lblProvincia);
		
		textField_6 = new JTextField();
		textField_6.setBounds(125, 153, 230, 20);
		alunnoP.add(textField_6);
		textField_6.setColumns(10);
		
		JLabel lblCap = new JLabel("CAP");
		lblCap.setBounds(18, 206, 46, 14);
		alunnoP.add(lblCap);
		
		textField_7 = new JTextField();
		textField_7.setBounds(125, 178, 230, 20);
		alunnoP.add(textField_7);
		textField_7.setColumns(10);
		
		JButton btnNewButton_1 = new JButton("INVIA");
		btnNewButton_1.setBounds(150, 283, 111, 44);
		alunnoP.add(btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConnectDB db = new ConnectDB(textField_1.getText(), textField_2.getText(), textField_3.getText(),comboBox_3.getSelectedItem(), textField_4.getText(), textField_5.getText() , textField_8.getText(), textField_6.getText(), textField_7.getText());
			}
		});
		
		jfrm.setVisible(true); 
		
		
	}
}
