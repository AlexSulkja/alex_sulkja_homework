
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ConnectDB {
 // JDBC driver name and database URL
 static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
 static final String DB_URL = "jdbc:mysql://localhost/scuolabua";

 //  Database credentials
 static final String USER = "root";
 static final String PASS = "";
 
 String nome, cognome, dataNascita, nazionalitaIT, comuneNascita, provincia, indirizzo, cap, numeroCell;

 
 public ConnectDB(String nome, String cognome, String dataNascita, Object object, String comuneNascita, String provincia, String indirizzo, String cap, String numeroCell) {
	this.nome = nome;
	this.cognome = cognome;
	this.dataNascita = dataNascita;
	nazionalitaIT = object.toString();
	this.comuneNascita = comuneNascita;
	this.provincia = provincia;
	this.indirizzo = indirizzo;
	this.cap = cap;
	this.numeroCell = numeroCell;
}
 
public boolean exec() {
	Connection conn = null;
	 Statement stmt = null;
	 boolean state = false;
	 try {
		 Class.forName("com.mysql.jdbc.Driver");
		 
		 System.out.println("Connecting to database...");
		 conn = DriverManager.getConnection(DB_URL,USER,PASS);
		 
		 System.out.println("Creating statement...");
		 stmt = conn.createStatement();
		 String sql = "INSERT INTO alunnoBua ( nome, cognome, dataNascita, nazionalitaIT, comuneNascita, provincia, indirizzo, cap, numeroCell) VALUES ('"+nome+"', '"+cognome+"', '"+dataNascita+"', '"+nazionalitaIT+"', '"+comuneNascita+"','"+provincia+"','"+indirizzo+"','"+cap+"','"+numeroCell+"')";
		System.out.println(sql);
		 state = stmt.execute(sql);
		 stmt.close();
		    conn.close();
		 }catch(SQLException se){
		    //Handle errors for JDBC
		    se.printStackTrace();
		 }catch(Exception e){
		    //Handle errors for Class.forName
		    e.printStackTrace();
		 }finally{
			 try{
			       if(stmt!=null)
			          stmt.close();
			    }catch(SQLException se2){
			    }// nothing we can do
			    try{
			       if(conn!=null)
			          conn.close();
			    }catch(SQLException se){
			       se.printStackTrace();
			    }//end finally try
			 
		 }
	
	return state;
}
}
