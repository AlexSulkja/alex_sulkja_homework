import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ConnectDB_1{
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	 static final String DB_URL = "jdbc:mysql://localhost/scuolabua";

	 //  Database credentials
	 static final String USER = "root";
	 static final String PASS = "";
	 
	 String annoS, classe, sezione, indirizzo;

	 
	 public ConnectDB_1(String annoS, Object object, Object object_1, Object object_2) {
		this.annoS = annoS;
		classe = object.toString();
		sezione = object.toString();
		indirizzo = object.toString();
	}
	 
	public boolean exec() {
		Connection conn = null;
		 Statement stmt = null;
		 boolean state = false;
		 try {
			 Class.forName("com.mysql.jdbc.Driver");
			 
			 System.out.println("Connecting to database...");
			 conn = DriverManager.getConnection(DB_URL,USER,PASS);
			 
			 System.out.println("Creating statement...");
			 stmt = conn.createStatement();
			 String sql = "INSERT INTO info (annoS, classe, sezione, indirizzo ) VALUES ('"+annoS+"', '"+classe+"', '"+sezione+"', '"+indirizzo+"')";
			System.out.println(sql);
			 state = stmt.execute(sql);
			 stmt.close();
			    conn.close();
			 }catch(SQLException se){
			    //Handle errors for JDBC
			    se.printStackTrace();
			 }catch(Exception e){
			    //Handle errors for Class.forName
			    e.printStackTrace();
			 }finally{
				 try{
				       if(stmt!=null)
				          stmt.close();
				    }catch(SQLException se2){
				    }// nothing we can do
				    try{
				       if(conn!=null)
				          conn.close();
				    }catch(SQLException se){
				       se.printStackTrace();
				    }//end finally try
				 
			 }
		
		return state;
	}
}
	