
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

import javax.swing.JOptionPane;

public class Client  {

	private ObjectOutputStream output;
	private ObjectInputStream input;
	private String message;
	private String server;
	private Socket connection;

	public Client()
	{
		try {

			// create Socket to make connection to server
			try {
				message = "tentativo di connessione";
				System.out.println(message);
				connection = new Socket( InetAddress.getByName("localhost"), 12345);
				// display connection information
				message =  "Connected to: " + 
						connection.getInetAddress().getHostName();
				System.out.println(message);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// set up output stream for objects
			try {
				output = new ObjectOutputStream( connection.getOutputStream() );
				output.flush(); // flush output buffer to send header information

				// set up input stream for objects
				input = new ObjectInputStream( connection.getInputStream() );

				message =  "Stream ottenuti";
				
				int n = Integer.parseInt(JOptionPane.showInputDialog("inserire numero"));
				System.out.println("invio numero");
				output.writeObject(n);
				String result = (String) input.readObject();
				System.out.println(result);
			    JOptionPane.showMessageDialog(null, result);
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}    

		}   

		finally {
			// Step 3: chiusura della connessione
			System.out.println("chiusura connessione");
			try {
				if(connection != null) connection.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				if(output != null) output.close();
			}
			catch( IOException ioException ) {
				ioException.printStackTrace();
			}   


			try {
				if(input != null)input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}





	} // end Client constructor

public static void main(String[] args) {
	new Client();
}

} // end class Client

